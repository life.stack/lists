import express from 'express'
import 'express-async-errors'
import { json } from 'body-parser'
import cookieSession from 'cookie-session'
import {
  errorHandler,
  NotFoundError,
  currentUser,
  livenessRouter,
} from '@life.stack/common'
import { newListRouter } from './routes/lists/newList'
import { indexListsRouter } from './routes/lists/indexLists'
import { appendListRouter } from './routes/lists/appendList'
import { showListRouter } from './routes/lists/showList'
import { updateListItemRouter } from './routes/items/updateListItem'
import { updateListRouter } from './routes/lists/updateList'
import { showItemRouter } from './routes/items/showItem'
import { toggleItemCheckedRouter } from './routes/items/toggleItemChecked'
import { appendsubitemRouter } from './routes/items/appendSubitem'
import { showSubitemRouter } from './routes/subitems/showSubitem'
import { updateSubitemRouter } from './routes/subitems/updateSubitem'
import { toggleSubitemCheckedRouter } from './routes/subitems/toggleSubitemChecked'
import { deleteListRouter } from './routes/lists/deleteList'

const app = express()
app.set('trust proxy', true) // App is behind nginx proxy and should trust it
app.use(json())
app.use(
  cookieSession({
    signed: false,
    secure: process.env.NODE_ENV !== 'test',
  })
)
app.use(currentUser)

//  App Specific
// /api/lists
app.use(newListRouter)
app.use(indexListsRouter)
app.use(appendListRouter)
app.use(showListRouter)
app.use(updateListRouter)
app.use(deleteListRouter)
// /api/items
app.use(updateListItemRouter)
app.use(showItemRouter)
app.use(toggleItemCheckedRouter)
app.use(appendsubitemRouter)
// /api/subitems
app.use(showSubitemRouter)
app.use(updateSubitemRouter)
app.use(toggleSubitemCheckedRouter)
// Extra
app.use(livenessRouter)

// If app routes don't work, throw a not found
app.all('*', () => {
  throw new NotFoundError()
})

// Error Handler
app.use(errorHandler)

export { app }
