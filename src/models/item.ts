import mongoose from 'mongoose'
import { updateIfCurrentPlugin } from 'mongoose-update-if-current'

// An interface that describes the properties that are required to create a new list
interface ItemAttrs {
  title: string
  note?: string
  groupId: string
}

// an interface that describes the properties a list document has
interface ItemDoc extends mongoose.Document {
  title: string
  note: string
  checked: boolean
  groupId: string
  subitems: Array<mongoose.Schema.Types.ObjectId>
}

// An interface that describes the properties that a list model has
interface ItemModel extends mongoose.Model<ItemDoc> {
  build(attrs: ItemAttrs): ItemDoc
  findByOwner(
    id: string,
    currentUser: { groups: string[] }
  ): Promise<ItemDoc | null>
}

const itemSchema = new mongoose.Schema(
  {
    groupId: {
      type: String,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    note: {
      type: String,
      default: '',
    },
    checked: {
      type: Boolean,
      default: false,
    },
    subitems: {
      type: [mongoose.Schema.Types.ObjectId],
      ref: 'Subitem',
      default: [],
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id
        delete ret._id
      },
    },
  }
)

itemSchema.set('versionKey', 'version')
itemSchema.plugin(updateIfCurrentPlugin)

itemSchema.statics.findByOwner = (itemId, currentUser) => {
  return Item.findOne({
    _id: itemId,
    groupId: currentUser.groups[0],
  }).populate('subitems')
}
itemSchema.statics.build = (attrs: ItemAttrs) => {
  return new Item(attrs)
}

const Item = mongoose.model<ItemDoc, ItemModel>('Item', itemSchema)

export { Item }
