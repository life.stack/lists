import mongoose from 'mongoose'
import { updateIfCurrentPlugin } from 'mongoose-update-if-current'

// An interface that describes the properties that are required to create a new list
interface ListAttrs {
  name: string
  groupId: string
}

// an interface that describes the properties a list document has
interface ListDoc extends mongoose.Document {
  name: string
  groupId: string
  version: number
  items: Array<mongoose.Schema.Types.ObjectId>
}

// An interface that describes the properties that a list model has
interface ListModel extends mongoose.Model<ListDoc> {
  build(attrs: ListAttrs): ListDoc
  findByOwner(
    id: string,
    currentUser: { groups: string[] }
  ): Promise<ListDoc | null>
}

const listSchema = new mongoose.Schema(
  {
    groupId: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
    },
    items: {
      type: [mongoose.Schema.Types.ObjectId],
      ref: 'Item',
      default: [],
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id
        delete ret._id
      },
    },
  }
)

listSchema.set('versionKey', 'version')
listSchema.plugin(updateIfCurrentPlugin)

listSchema.statics.findByOwner = (listId, currentUser) => {
  return List.findOne({
    _id: listId,
    groupId: currentUser.groups[0],
  }).populate({ path: 'items', populate: { path: 'subitems' } })
}
listSchema.statics.build = (attrs: ListAttrs) => {
  return new List(attrs)
}

const List = mongoose.model<ListDoc, ListModel>('List', listSchema)

export { List }
