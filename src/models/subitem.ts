import mongoose from 'mongoose'
import { updateIfCurrentPlugin } from 'mongoose-update-if-current'
import { Item } from './item'

// An interface that describes the properties that are required to create a new subitem
interface SubitemAttrs {
  title: string
  note?: string
  groupId: string
}

// an interface that describes the properties a subitem document has
interface SubitemDoc extends mongoose.Document {
  title: string
  note: string
  checked: boolean
  groupId: string
}

// An interface that describes the properties that a list model has
interface SubitemModel extends mongoose.Model<SubitemDoc> {
  build(attrs: SubitemAttrs): SubitemDoc
  findByOwner(
    id: string,
    currentUser: { groups: string[] }
  ): Promise<SubitemDoc | null>
}

const subitemSchema = new mongoose.Schema(
  {
    groupId: {
      type: String,
      required: true,
    },
    title: {
      type: String,
      required: true,
    },
    note: {
      type: String,
      default: '',
    },
    checked: {
      type: Boolean,
      default: false,
    },
  },
  {
    toJSON: {
      transform(doc, ret) {
        ret.id = ret._id
        delete ret._id
      },
    },
  }
)

subitemSchema.set('versionKey', 'version')
subitemSchema.plugin(updateIfCurrentPlugin)

subitemSchema.statics.findByOwner = (subitemId, currentUser) => {
  return Subitem.findOne({
    _id: subitemId,
    groupId: currentUser.groups[0],
  })
}

subitemSchema.statics.build = (attrs: SubitemAttrs) => {
  return new Subitem(attrs)
}

const Subitem = mongoose.model<SubitemDoc, SubitemModel>(
  'Subitem',
  subitemSchema
)

export { Subitem }
