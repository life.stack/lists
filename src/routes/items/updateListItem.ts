import express, { Request, Response } from 'express'
import {
  currentUser,
  NotFoundError,
  requireAuth,
  validateRequest,
} from '@life.stack/common'
import { body } from 'express-validator'
import { Item } from '../../models/item'

const router = express.Router()

router.put(
  '/api/items/:itemId',
  currentUser,
  requireAuth,
  [
    body('title').not().isEmpty().withMessage('title is required'),
    body('checked').toBoolean(),
    body('note').isLength({ min: 0, max: 4096 }),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const { listId, itemId } = req.params
    const { title, note, checked } = req.body

    const updatedItem = {
      title,
      note,
      checked,
    }

    const item = await Item.findByOwner(itemId, req.currentUser!)
    if (!item) {
      throw new NotFoundError()
    }

    item.set({
      title,
      note,
      checked,
    })
    await item.save()

    res.send(item)
  }
)

export { router as updateListItemRouter }
