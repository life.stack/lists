import express, { Request, Response } from 'express'
import { currentUser, NotFoundError, requireAuth } from '@life.stack/common'
import { Item } from '../../models/item'

const router = express.Router()

router.get(
  '/api/items/:itemId',
  currentUser,
  requireAuth,
  async (req: Request, res: Response) => {
    const { itemId } = req.params

    const item = await Item.findByOwner(itemId, req.currentUser!)
    if (!item) {
      throw new NotFoundError()
    }

    res.send(item)
  }
)

export { router as showItemRouter }
