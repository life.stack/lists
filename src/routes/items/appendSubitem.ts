import express, { Request, Response } from 'express'
import {
  BadRequestError,
  requireAuth,
  validateRequest,
  currentUser,
} from '@life.stack/common'
import { body } from 'express-validator'
import { List } from '../../models/list'
import { Item } from '../../models/item'
import { Subitem } from '../../models/subitem'

const router = express.Router()

type AuthRequest = Request & { currentUser: { groups: Array<string> } }

router.post(
  '/api/items/:id/subitem',
  currentUser,
  requireAuth,
  [body('title').not().isEmpty().withMessage('title must be provided')],
  validateRequest,
  async (req: Request, res: Response) => {
    const { id } = req.params
    const { title } = req.body
    const groupId = req?.currentUser?.groups[0]

    const item = await Item.findByOwner(id, req.currentUser!)
    if (!item || !groupId) {
      throw new BadRequestError('List not found')
    }

    const subitem = Subitem.build({ title, groupId })
    await subitem.save()

    item.subitems.push(subitem.id)
    await item.save()

    res.status(201).send(subitem)
  }
)

export { router as appendsubitemRouter }
