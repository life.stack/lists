import request from 'supertest'
import { app } from '../../../app'

it('has a route handler on PUT /api/items/:itemId', async () => {
  const user = global.signin()

  const { body: list } = await request(app)
    .post('/api/lists')
    .set('Cookie', user)
    .send({ name: 'list' })
    .expect(201)

  const item = {
    title: 'test',
    note: 'testnote',
  }

  const { body: itemAdd } = await request(app)
    .post(`/api/lists/${list.id}`)
    .set('Cookie', user)
    .send(item)
    .expect(201)

  const updatedItem = {
    title: 'update',
    note: 'updatednote',
    checked: true,
  }

  const { body: listUpdate } = await request(app)
    .put(`/api/items/${itemAdd.id}`)
    .set('Cookie', user)
    .send(updatedItem)
    .expect(200)

  expect(listUpdate.title).toEqual(updatedItem.title)
  expect(listUpdate.note).toEqual(updatedItem.note)
  expect(listUpdate.checked).toEqual(updatedItem.checked)
})
