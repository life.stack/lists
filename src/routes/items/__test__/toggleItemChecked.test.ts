import request from 'supertest'
import { app } from '../../../app'
import mongoose from 'mongoose'

it('has a route handler on PUT /api/items/:itemId/checked', async () => {
  const user = global.signin()

  const { body: list } = await request(app)
    .post('/api/lists')
    .set('Cookie', user)
    .send({ name: 'list' })
    .expect(201)

  const item = {
    title: 'test',
    note: 'testnote',
  }

  const { body: itemAdd } = await request(app)
    .post(`/api/lists/${list.id}`)
    .set('Cookie', user)
    .send(item)
    .expect(201)

  const updatedItem = {
    checked: true,
  }

  const { body: listUpdate } = await request(app)
    .put(`/api/items/${itemAdd.id}/checked`)
    .set('Cookie', user)
    .send(updatedItem)
    .expect(200)

  expect(listUpdate.id).toEqual(itemAdd.id)
  expect(listUpdate.checked).toEqual(updatedItem.checked)
})

it('requires authentication', async () => {
  await request(app)
    .put(`/api/items/${mongoose.Types.ObjectId().toHexString()}/checked`)
    .send({})
    .expect(401)
})
