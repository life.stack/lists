import request from 'supertest'
import { app } from '../../../app'

it('has a route handler on GET /api/items/:itemId', async () => {
  const user = global.signin()

  const { body: list } = await request(app)
    .post('/api/lists')
    .set('Cookie', user)
    .send({ name: 'list' })
    .expect(201)

  const item = {
    title: 'test',
    note: 'testnote',
  }

  const { body: itemAdd } = await request(app)
    .post(`/api/lists/${list.id}`)
    .set('Cookie', user)
    .send(item)
    .expect(201)

  const { body: showItem } = await request(app)
    .get(`/api/items/${itemAdd.id}`)
    .set('Cookie', user)
    .send()
    .expect(200)

  expect(itemAdd.id).toEqual(showItem.id)
  expect(itemAdd.title).toEqual(showItem.title)
  expect(itemAdd.note).toEqual(showItem.note)
  expect(itemAdd.checked).toEqual(showItem.checked)
})
