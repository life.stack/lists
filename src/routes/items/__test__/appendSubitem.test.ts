import request from 'supertest'
import { app } from '../../../app'
import mongoose from 'mongoose'

it('has a route handler listening to /api/items/:id/subitem for POST requests', async () => {
  const response = await request(app)
    .post(`/api/items/${mongoose.Types.ObjectId().toHexString()}/subitem`)
    .send({})

  expect(response.status).not.toEqual(404)
})

it('can only be accessed by a signed in user', async () => {
  await request(app)
    .post(`/api/items/${mongoose.Types.ObjectId().toHexString()}/subitem`)
    .send({})
    .expect(401)
})

it('returns a status other than 401 if the user is signed in', async () => {
  const response = await request(app)
    .post(`/api/items/${mongoose.Types.ObjectId().toHexString()}/subitem`)
    .set('Cookie', global.signin())
    .send({})

  expect(response.status).not.toEqual(401)
})

it('fails when item does not exist', async () => {
  await request(app)
    .post(`/api/items/${mongoose.Types.ObjectId().toHexString()}/subitem`)
    .set('Cookie', global.signin())
    .send({})
    .expect(400)
})

it('appends to an items list of subitems', async () => {
  const user = global.signin()

  const { body: aList } = await request(app)
    .post('/api/lists')
    .set('Cookie', user)
    .send({ name: 'A list' })
    .expect(201)

  const { body: firstItem } = await request(app)
    .post(`/api/lists/${aList.id}`)
    .set('Cookie', user)
    .send({ title: 'atitle', note: 'anote' })
    .expect(201)

  const subitem = { title: 'subitem' }

  const { body: firstSubitem } = await request(app)
    .post(`/api/items/${firstItem.id}/subitem`)
    .set('Cookie', user)
    .send(subitem)
    .expect(201)

  expect(firstSubitem.title).toEqual(subitem.title)
  expect(firstSubitem.checked).toEqual(false)

  const { body: secondSubitem } = await request(app)
    .post(`/api/items/${firstItem.id}/subitem`)
    .set('Cookie', user)
    .send({ title: 'another subitem' })
    .expect(201)

  const { body: updatedItem } = await request(app)
    .get(`/api/items/${firstItem.id}`)
    .set('Cookie', user)
    .send()
    .expect(200)

  expect(updatedItem.subitems.length).toEqual(2)
})
