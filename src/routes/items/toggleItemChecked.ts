import express, { Request, Response } from 'express'
import { NotFoundError, requireAuth, validateRequest } from '@life.stack/common'
import { body } from 'express-validator'
import { Item } from '../../models/item'

const router = express.Router()

router.put(
  '/api/items/:itemId/checked',
  requireAuth,
  [body('checked').toBoolean()],
  validateRequest,
  async (req: Request, res: Response) => {
    const { itemId } = req.params
    const { checked } = req.body

    const item = await Item.findByOwner(itemId, req?.currentUser!)
    if (!item) {
      throw new NotFoundError()
    }

    item.set({
      checked,
    })
    await item.save()

    res.send(item)
  }
)

export { router as toggleItemCheckedRouter }
