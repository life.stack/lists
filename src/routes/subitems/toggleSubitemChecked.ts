import express, { Request, Response } from 'express'
import {
  currentUser,
  NotFoundError,
  requireAuth,
  validateRequest,
} from '@life.stack/common'
import { body } from 'express-validator'
import { Subitem } from '../../models/subitem'

const router = express.Router()

router.put(
  '/api/subitems/:subitemId/checked',
  currentUser,
  requireAuth,
  [body('checked').toBoolean()],
  validateRequest,
  async (req: Request, res: Response) => {
    const { subitemId } = req.params
    const { checked } = req.body

    const subitem = await Subitem.findByOwner(subitemId, req?.currentUser!)
    if (!subitem) {
      throw new NotFoundError()
    }

    subitem.set({
      checked,
    })
    await subitem.save()

    res.send(subitem)
  }
)

export { router as toggleSubitemCheckedRouter }
