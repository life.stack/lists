import express, { Request, Response } from 'express'
import { currentUser, NotFoundError, requireAuth } from '@life.stack/common'
import { Subitem } from '../../models/subitem'

const router = express.Router()

router.get(
  '/api/subitems/:subitemId',
  currentUser,
  requireAuth,
  async (req: Request, res: Response) => {
    const { itemId: subitemId } = req.params

    const subitem = await Subitem.findByOwner(subitemId, req.currentUser!)
    if (!subitem) {
      throw new NotFoundError()
    }

    res.send(subitem)
  }
)

export { router as showSubitemRouter }
