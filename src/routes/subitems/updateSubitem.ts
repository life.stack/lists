import express, { Request, Response } from 'express'
import {
  currentUser,
  NotFoundError,
  requireAuth,
  validateRequest,
} from '@life.stack/common'
import { body } from 'express-validator'
import { Subitem } from '../../models/subitem'

const router = express.Router()

router.put(
  '/api/subitems/:subitemId',
  currentUser,
  requireAuth,
  [
    body('title').not().isEmpty().withMessage('title is required'),
    body('checked').toBoolean(),
    body('note').isLength({ min: 0, max: 4096 }),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const { subitemId } = req.params
    const { title, note, checked } = req.body

    const updatedItem = {
      title,
      note,
      checked,
    }

    const subitem = await Subitem.findByOwner(subitemId, req.currentUser!)
    if (!subitem) {
      throw new NotFoundError()
    }

    subitem.set({
      title,
      note,
      checked,
    })
    await subitem.save()

    res.send(subitem)
  }
)

export { router as updateSubitemRouter }
