import express, { Request, Response } from 'express'
import { requireAuth, validateRequest } from '@life.stack/common'
import { body } from 'express-validator'
import { List } from '../../models/list'

const router = express.Router()

router.post(
  '/api/lists',
  requireAuth,
  [
    body('name')
      .not()
      .isEmpty()
      .isLength({ min: 1, max: 256 })
      .withMessage('name must be provided'),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const { name } = req.body

    const list = await List.build({
      name,
      groupId: req.currentUser!.groups[0],
    })
    await list.save()

    res.status(201).send(list)
  }
)

export { router as newListRouter }
