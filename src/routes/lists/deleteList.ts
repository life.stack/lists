import express, { Request, Response } from 'express'
import { currentUser, NotFoundError, requireAuth } from '@life.stack/common'
import { List } from '../../models/list'

const router = express.Router()

router.delete(
  '/api/lists/:listId',
  currentUser,
  requireAuth,
  async (req: Request, res: Response) => {
    const { listId } = req.params

    const list = await List.findByOwner(listId, req.currentUser!)

    if (!list) {
      throw new NotFoundError()
    }

    await list.remove()

    res.status(202).send({})
  }
)

export { router as deleteListRouter }
