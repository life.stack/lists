import express, { Request, Response } from 'express'
import { List } from '../../models/list'
import { requireAuth } from '@life.stack/common'

const router = express.Router()

router.get('/api/lists', requireAuth, async (req: Request, res: Response) => {
  const lists = await List.find({
    groupId: req.currentUser!.groups[0],
  }).populate('items')

  res.send(lists)
})

export { router as indexListsRouter }
