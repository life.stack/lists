import request from 'supertest'
import { app } from '../../../app'
import mongoose from 'mongoose'

const validList = {
  name: 'A List',
}

it('route requires authentication', async () => {
  const id = new mongoose.Types.ObjectId().toHexString()
  await request(app).get(`/api/lists/${id}`).send().expect(401)
})

it('returns a 404 if the list is not found', async () => {
  const id = new mongoose.Types.ObjectId().toHexString()
  await request(app)
    .get(`/api/lists/${id}`)
    .set('Cookie', global.signin())
    .send()
    .expect(404)
})

it('returns the list if the list is found', async () => {
  const user = global.signin()
  // Create a List
  const { body: newList } = await request(app)
    .post('/api/lists')
    .set('Cookie', user)
    .send(validList)
    .expect(201)

  const { body: listSearch } = await request(app)
    .get(`/api/lists/${newList.id}`)
    .set('Cookie', user)
    .send()
    .expect(200)

  expect(listSearch.name).toEqual(validList.name)
})
