import request from 'supertest'
import { app } from '../../../app'
import mongoose from 'mongoose'

it('has a route handler listening to /api/lists/:id for POST requests', async () => {
  const response = await request(app)
    .post(`/api/lists/${mongoose.Types.ObjectId().toHexString()}`)
    .send({})

  expect(response.status).not.toEqual(404)
})

it('can only be accessed by a signed in user', async () => {
  await request(app)
    .post(`/api/lists/${mongoose.Types.ObjectId().toHexString()}`)
    .send({})
    .expect(401)
})

it('returns a status other than 401 if the user is signed in', async () => {
  const response = await request(app)
    .post(`/api/lists/${mongoose.Types.ObjectId().toHexString()}`)
    .set('Cookie', global.signin())
    .send({})

  expect(response.status).not.toEqual(401)
})

it('fails when list does not exist', async () => {
  await request(app)
    .post(`/api/lists/${mongoose.Types.ObjectId().toHexString()}`)
    .set('Cookie', global.signin())
    .send({})
    .expect(400)
})

it('appends to an existing list', async () => {
  const user = global.signin()

  const { body: aList } = await request(app)
    .post('/api/lists')
    .set('Cookie', user)
    .send({ name: 'A list' })
    .expect(201)

  const anItem = { title: 'atitle', note: 'anote' }

  const { body: firstAppend } = await request(app)
    .post(`/api/lists/${aList.id}`)
    .set('Cookie', user)
    .send(anItem)
    .expect(201)

  expect(firstAppend.title).toEqual(anItem.title)
  expect(firstAppend.note).toEqual(anItem.note)
  expect(firstAppend.checked).toEqual(false)

  const { body: secondAppend } = await request(app)
    .post(`/api/lists/${aList.id}`)
    .set('Cookie', user)
    .send({ title: 'another item' })
    .expect(201)

  const { body: updatedList } = await request(app)
    .get(`/api/lists/${aList.id}`)
    .set('Cookie', user)
    .send()
    .expect(200)

  expect(updatedList.items.length).toEqual(2)
})
