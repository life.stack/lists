import request from 'supertest'
import { app } from '../../../app'
import mongoose from 'mongoose'
import { List } from '../../../models/list'

it('has a route handler listening to /api/lists/:id for DELETE requests', async () => {
  const { body: createdList } = await request(app)
    .post('/api/lists')
    .set('Cookie', global.signin())
    .send({ name: 'a list name' })
    .expect(201)

  const response = await request(app)
    .delete(`/api/lists/${createdList.id}`)
    .send({})

  expect(response.status).not.toEqual(404)
})

it('can only be accessed by a signed in user', async () => {
  await request(app)
    .delete(`/api/lists/${mongoose.Types.ObjectId().toHexString()}`)
    .send({})
    .expect(401)
})

it('returns a status other than 401 if the user is signed in', async () => {
  const response = await request(app)
    .delete(`/api/lists/${mongoose.Types.ObjectId().toHexString()}`)
    .set('Cookie', global.signin())
    .send({})

  expect(response.status).not.toEqual(401)
})

it('deletes an existing list', async () => {
  const user = global.signin()

  const { body: createdList } = await request(app)
    .post('/api/lists')
    .set('Cookie', user)
    .send({ name: 'a list name' })
    .expect(201)

  const deleteResponse = await request(app)
    .delete(`/api/lists/${createdList.id}`)
    .set('Cookie', user)
    .send()
    .expect(202)

  await request(app)
    .get(`/api/lists/${createdList.id}`)
    .set('Cookie', user)
    .send()
    .expect(404)

  const listFromDb = await List.findById(createdList.id)
  expect(listFromDb).toEqual(null)
})
