import request from 'supertest'
import { app } from '../../../app'

it('has a route handler on PUT /api/lists/:listId', async () => {
  const user = global.signin()

  const { body: list } = await request(app)
    .post('/api/lists')
    .set('Cookie', user)
    .send({ name: 'list' })
    .expect(201)

  const item = {
    title: 'test',
    note: 'testnote',
  }

  const { body: itemAdd } = await request(app)
    .post(`/api/lists/${list.id}`)
    .set('Cookie', user)
    .send(item)
    .expect(201)

  const updatedList = {
    name: 'a new name',
  }

  const { body: listUpdate } = await request(app)
    .put(`/api/lists/${list.id}`)
    .set('Cookie', user)
    .send(updatedList)
    .expect(200)

  expect(listUpdate.name).toEqual(updatedList.name)
  expect(listUpdate.items).toEqual([itemAdd.id])
})
