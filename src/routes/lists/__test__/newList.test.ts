import request from 'supertest'
import { app } from '../../../app'
import { List } from '../../../models/list'

it('has a route handler listening to /api/lists for POST requests', async () => {
  const response = await request(app).post('/api/lists').send({})

  expect(response.status).not.toEqual(404)
})

it('can only be accessed by a signed in user', async () => {
  await request(app).post('/api/lists').send({}).expect(401)
})

it('returns a status other than 401 if the user is signed in', async () => {
  const response = await request(app)
    .post('/api/lists')
    .set('Cookie', global.signin())
    .send({})

  expect(response.status).not.toEqual(401)
})

it('must be provided a name', async () => {
  await request(app)
    .post('/api/lists')
    .set('Cookie', global.signin())
    .send({})
    .expect(400)

  await request(app)
    .post('/api/lists')
    .set('Cookie', global.signin())
    .send({ name: null })
    .expect(400)

  await request(app)
    .post('/api/lists')
    .set('Cookie', global.signin())
    .send({ name: '' })
    .expect(400)

  const { body: createdList } = await request(app)
    .post('/api/lists')
    .set('Cookie', global.signin())
    .send({ name: 'a list name' })
    .expect(201)

  const listInDatabase = await List.findById(createdList.id)
  expect(listInDatabase).not.toEqual(null)
  expect(listInDatabase?.id).toEqual(createdList.id)
  expect(listInDatabase?.name).toEqual(createdList.name)
})
