import request from 'supertest'
import { app } from '../../../app'

it('has a GET route on /api/lists', async () => {
  const response = await request(app)
    .get('/api/lists')
    .set('Cookie', global.signin())
    .send()

  expect(response.status).not.toEqual(404)
})

it('returns a list of Lists for the current user', async () => {
  // Create two users
  const userOne = global.signin()
  const userTwo = global.signin()

  // Create one list as User #1
  await request(app)
    .post('/api/lists')
    .set('Cookie', userOne)
    .send({ name: 'My First List' })
    .expect(201)

  // Create two lists as User #2
  const { body: listOne } = await request(app)
    .post('/api/lists')
    .set('Cookie', userTwo)
    .send({ name: 'Shopping' })
    .expect(201)
  const { body: listTwo } = await request(app)
    .post('/api/lists')
    .set('Cookie', userTwo)
    .send({ name: 'Presents' })
    .expect(201)

  // Fetch lists for User #2
  const response = await request(app)
    .get('/api/lists')
    .set('Cookie', userTwo)
    .expect(200)

  // Expect only 2 orders from User #2 are returned
  expect(response.body.length).toEqual(2)
  expect(response.body[0].id).toEqual(listOne.id)
  expect(response.body[1].id).toEqual(listTwo.id)
})
