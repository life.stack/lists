import express, { Request, Response } from 'express'
import { List } from '../../models/list'
import { currentUser, NotFoundError, requireAuth } from '@life.stack/common'

const router = express.Router()

router.get(
  '/api/lists/:id',
  currentUser,
  requireAuth,
  async (req: Request, res: Response) => {
    const list = await List.findByOwner(req.params.id, req.currentUser!)
    if (!list) {
      throw new NotFoundError()
    }

    res.send(list)
  }
)

export { router as showListRouter }
