import express, { Request, Response } from 'express'
import {
  BadRequestError,
  requireAuth,
  validateRequest,
} from '@life.stack/common'
import { body } from 'express-validator'
import { List } from '../../models/list'

const router = express.Router()

router.put(
  '/api/lists/:listId',
  requireAuth,
  [body('name').not().isEmpty().withMessage('name is required')],
  validateRequest,
  async (req: Request, res: Response) => {
    const { listId } = req.params
    const { name } = req.body

    const list = await List.findOneAndUpdate(
      { _id: listId },
      { $set: { name: name } },
      {
        new: true,
      }
    )

    if (!list) {
      throw new BadRequestError('Unable to update list')
    }

    res.send(list)
  }
)

export { router as updateListRouter }
