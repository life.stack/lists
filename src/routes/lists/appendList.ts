import express, { Request, Response } from 'express'
import {
  BadRequestError,
  requireAuth,
  validateRequest,
  currentUser,
} from '@life.stack/common'
import { body } from 'express-validator'
import { List } from '../../models/list'
import { Item } from '../../models/item'

const router = express.Router()

type AuthRequest = Request & { currentUser: { groups: Array<string> } }

router.post(
  '/api/lists/:id',
  currentUser,
  requireAuth,
  [
    body('title').not().isEmpty().withMessage('title must be provided'),
    body('note').isLength({ min: 0, max: 4096 }),
  ],
  validateRequest,
  async (req: Request, res: Response) => {
    const { id } = req.params
    const { title, note } = req.body
    const groupId = req?.currentUser?.groups[0]

    const list = await List.findByOwner(id, req.currentUser!)
    if (!list || !groupId) {
      throw new BadRequestError('List not found')
    }

    const item = Item.build({ title, note, groupId })
    await item.save()

    list.items.push(item.id)
    await list.save()

    res.status(201).send(item)
  }
)

export { router as appendListRouter }
